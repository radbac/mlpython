# README #

Hi and welcome here. Hope you will find something useful for you.

### What is this repository for? ###

* This repository is preparation and learning material for Data Science, Deep Learning, & Machine Learning with Python course on: https://www.udemy.com/data-science-and-machine-learning-with-python-hands-on/

### How do I get set up? ###
* Follow instructions from http://sundog-education.com/datascience/ (download files should be found there also)
* Set up - very simple: read and download VM image for [Hortonworks data platform](https://hortonworks.com/products/data-center/hdp/)


### Contacts ###

For any additional info, feel free ping me:

* [Radovan Baćović (LinkedIn profile)](https://www.linkedin.com/in/radovan-ba%C4%87ovi%C4%87-6498603/) 
* or drop an email to: radovan.bacovic@gmail.com