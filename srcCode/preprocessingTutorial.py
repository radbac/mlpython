from sklearn.preprocessing import scale
from sklearn.cluster import KMeans
import numpy as np

X_train = np.array([[1.0, -1.0, 2.0],
                    [2.0, 0.0, 0.0],
                    [0.0, 1.0, -1.0]])
print(X_train)
print(X_train.mean(axis=0))
print(X_train.std(axis=0))

X_scaled = scale(X_train)
print(X_scaled)
print(X_scaled.mean(axis=0))
print(X_scaled.std(axis=0))

X = np.array([[1., 2.], [1., 4.], [1., 0.], [4., 2.], [4., 4.], [4., 0.]])

#kmeans = KMeans(n_clusters=2, random_state=0).fit(scale(X))
kmeans = KMeans(n_clusters=2, random_state=0).fit(X)

print(kmeans.labels_)
s