from sklearn.feature_extraction.text import CountVectorizer

vectorizer = CountVectorizer()#stop_words={'This'})

corpus = [
     'This is the first document.',
     'This is the second second document.',
     'And the third one.',
     'Is this the first document?']

print(corpus)

X = vectorizer.fit_transform(corpus)

print(vectorizer.vocabulary_)
print(vectorizer.get_feature_names())
print(X.toarray())

analyze = vectorizer.build_analyzer()

print(analyze("This is a text document to analyze.") == (['this', 'is', 'text', 'document', 'to', 'analyze']))

print(vectorizer.vocabulary_.get('this'))