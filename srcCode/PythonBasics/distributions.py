import numpy as np
import matplotlib.pyplot as plt

# values = np.random.uniform(-10, 10, 100000)
# print(values[:5])
# plt.hist(values,50)
# plt.show()

from scipy.stats import norm
# x = np.arange(-3, 3, 0.01)
#
# plt.plot(x, norm.pdf(x))
# plt.show()

# mu = 5.2
# sigma = 2.0
#
# values = np.random.normal(mu, sigma, 10000)
# plt.hist(values,50)
# plt.show()

# from scipy.stats import expon
#
# x = np.arange(0, 10, 0.001)
#
# plt.plot(x, expon.pdf(x))
# plt.show()

# from scipy.stats import binom
#
# n, p = 10, 0.5
# x = np.arange(0, 10, 0.01)
# print(len(x))
# plt.plot(x, binom.pmf(x, n, p))
# plt.show()

from scipy.stats import poisson

mu = 500
x = np.arange(400,600, 0.5)
plt.plot(x, poisson.pmf(x, mu))
plt.show()