import os
import numpy as np
import pandas as pd
import matplotlib

current_file = os.getcwd() + "\datafiles\PastHires.csv"

df = pd.read_csv(current_file)
print(df.shape)
print(df.columns)

print(df['Hired'])
print(df['Hired'][5])
print(df[['Hired', 'Years Experience']][:5])

print(df.sort_values(['Years Experience']))

degree_counts = df['Level of Education'].value_counts()

print(degree_counts)

degree_counts.plot(kind='bar')
