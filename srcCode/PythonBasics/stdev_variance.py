import numpy as np
import matplotlib.pyplot as plt

incomes = np.random.normal(100.00, 20.0, 10000)

print(incomes.std())
print(incomes.var())
plt.hist(incomes, 10)
plt.show()

