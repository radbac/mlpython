import numpy as np
import matplotlib.pyplot as plt

values = np.random.normal(0, 0.5, 10000)
#
# plt.hist(values, 50)
# plt.show()
#
# print(np.percentile(values, 50))
print('Mean: %s' % np.mean(values))
print('Variance: %s' % np.var(values))

import scipy.stats as sp
print('Skew %s' % sp.skew(values))
print('Kurtosis %s' % sp.kurtosis(values))
# print(np.median(values))
# print(np.percentile(values, 70))
# print(np.percentile(values, 90))

