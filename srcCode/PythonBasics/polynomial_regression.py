import numpy as np
import matplotlib.pyplot as plt
from sklearn.metrics import r2_score

np.random.seed(2)

page_speed = np.random.normal(3.0, 1.0, 1000)

purchase_amount = np.random.normal(50.0, 10.0, 1000) / page_speed


x = np.array(page_speed)
y = np. array(purchase_amount)

p4 = np.poly1d(np.polyfit(x, y, 4))
xp = np.linspace(0, 7, 100)

r2 = r2_score(y, p4(x))
print(r2)
plt.scatter(page_speed, purchase_amount)
plt.plot(xp, p4(xp), c='r')
plt.title('Linear regression example')
plt.xlabel('Page speed')
plt.ylabel('Purchase amount $')


plt.show()