import json
from typing import Dict
from datetime import datetime

def calc_setting(calc_name: str) -> dict:
    """
    :type calc_name: str
    """
    with open('%s.json' % calc_name, 'r') as outfile:
        return json.load(outfile)


def ulazne_vrednosti(**kwargs: object) -> dict:
    element_value: Dict[str, object] = {}
    for key, value in kwargs.items():
        element_value[key] = value
    return element_value


def obracunaj(roba, neto_kolicina: int, elements: dict, calc_definition: dict) -> dict:
    """
    :type roba: dict
    :type neto_kolicina: int
    :type elements: dict
    :type calc_definition: dict
    """
    assert type(roba) == dict
    assert neto_kolicina > 0
    assert type(elements) == dict
    assert type(calc_definition) == dict

    ########################################
    # JUS
    ########################################
    kolicina = neto_kolicina
    jus = kolicina
    jus_korekcija = 0
    usluge_korekcija = 0
    za_isplatu = 0

    obracun = dict(Roba=roba,
                   Kolicine={'Neto': kolicina,
                             'JUS Korekcija': jus_korekcija,
                             'JUS': jus,
                             'Korekcija': usluge_korekcija,
                             'Za isplatu': za_isplatu},
                   Analiza={'NETO': {}, 'JUS': {}},
                   Meta={'Datum': str(datetime.now())})

    for k, v in calc_definition.get("Elements").get('JUS').items():
        unos = str(elements.get(k))
        stmt = v.get('formula').replace('UNOS', unos).replace('KOLICINA', str(kolicina))

        skinuto = eval(stmt)
        jus += skinuto
        jus_korekcija += skinuto
        obracun['Analiza']['NETO'][k] = dict(Naziv=v.get('caption'),
                                             Izmereno=unos,
                                             Standard=v.get('standard'),
                                             Korekcija=skinuto)

    obracun['Kolicine']['JUS Korekcija'] = jus_korekcija
    obracun['Kolicine']['JUS'] = jus

    ########################################
    # USLUGE
    ########################################
    za_isplatu = jus

    for k, v in calc_definition.get("Elements").get('USLUGE').items():
        vrednost_od = v.get('vrednost_od')
        if vrednost_od is None:
            unos = v.get('standard')
        else:
            unos = obracun.get('Analiza', '{}').get('NETO', '{}').get(vrednost_od, '{}').get('Izmereno', 0)

        if v.get('osnovica') == 'JUS':
            kolicina = obracun.get('Kolicine').get('JUS')

        stmt = v.get('formula').replace('UNOS', unos).replace('KOLICINA', str(kolicina))
        skinuto = eval(stmt)
        # codeBlock = '''stmt'''
        # compiledCodeBlock = compile(codeBlock, '<string>', 'single')
        # eval(compiledCodeBlock)
        usluge_korekcija += skinuto
        za_isplatu += skinuto
        obracun['Analiza']['JUS'][k] = {'Naziv': v.get('caption'),
                                        'Izmereno': unos,
                                        'Standard': v.get('standard'),
                                        'Korekcija': skinuto}

    obracun['Kolicine']['Usluge Korekcija'] = usluge_korekcija
    obracun['Kolicine']['Za isplatu'] = za_isplatu

    return obracun


def main():
    kolicina = 10000
    naziv_robe = dict(Naziv='PSENICA ROD 2020',
                      Sifra='1001',
                      Rod=2020)
    definicija_tablice = calc_setting('demo1')
    izmerene_vrednosti = ulazne_vrednosti(VLAGA=16, HEKTOLITAR=78, PRIMESE=2, LOM=1.8)
    obracun = obracunaj(naziv_robe, kolicina, izmerene_vrednosti, definicija_tablice)

    print(json.dumps(obracun, indent=4)) #, sort_keys=True))


if __name__ == "__main__":
    main()
