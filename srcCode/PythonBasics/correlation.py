import numpy as np
import matplotlib.pyplot as plt

def de_mean(x):
    xmean = np.mean(x)
    return [xi - xmean for xi in x]


def covariance(x,y):
    n = len(x)

    return np.dot(de_mean(x), de_mean(y)) / (n - 1)


page_speed = np.random.normal(3.0, 1, 1000)

purchase_amount = np.random.normal(50.0, 10.0, 1000) / page_speed

# print(covariance(page_speed, purchase_amount))
# plt.scatter(page_speed, purchase_amount)
# plt.show()

print(np.corrcoef(page_speed,purchase_amount))