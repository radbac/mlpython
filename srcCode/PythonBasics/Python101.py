'''Part 1'''
# Arrays
listOfNumbers = [1,2,3,4,5,6]

print(listOfNumbers)

for number in listOfNumbers:
    print(number)
    if number % 2 == 0:
        print("is even")
    else:
        print("is odd")

print ("All done!")

import numpy as np

A = np.random.normal(25.0,5.0,10)

print(A)

x = [1,2,3,4,5]

print (x)

x.extend([1,4])

print(x)

x.sort(reverse=True)
print(x)

# Tuples
x = (1,2,3)
y = (4,5,6)
z = (x,y)
print(z)
(age,income) = '22,1232.00'.split(',')
print(income)

#Dictionaries
captains = {}

captains["Enteprise"] = "Kirk"
captains["Voyager"] = "2"
captains["341"] = "Zoki"
print(captains.get("s"))

for ship in captains:
    print (ship + " "+captains[ship])


'''Part 1'''
# Functions

def square(x):
    return x ** 2

print(square(2))


# boolean
print(1 is not 3)

# Looping

for i in range(10):
    print(i)