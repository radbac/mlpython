from scipy.stats import norm
import matplotlib.pyplot as plt
import numpy as np

x = np.arange(-3,3,0.001)

axes = plt.axes()
axes.set_xlim([-5,5])
axes.set_ylim([0,1.0])
axes.set_xticks([-5,-4,-3,-2,-1,0,1,2,3,4,5])
axes.set_yticks([0,0.1,0.2,0.3,0.4,0.5,0.6,0.7,0.8,0.9,1])
axes.grid()

# plt.xkcd()
plt.xlabel('Greebles')
plt.ylabel('Probability')
plt.plot(x, norm.pdf(x),'g:')
plt.plot(x, norm.pdf(x, 1.0, .5),'r--')
plt.legend(['Sneetches','Gacks'],loc=1)
plt.show()

# plt.savefig('test.png', format='png')

plt.rcdefaults
values = [12,55,4,32,14]
colors = ['r','g','b','c','m']
explode = [0,0,0.2,0,0]
labels = ['India','United States','Russia','China','Europe']

plt.pie(values,colors=colors,labels=labels,explode=explode)
plt.title('Students location')
plt.show()


