from numpy import random
random.seed(0)

totals = {20:0, 30:0, 40:0, 50:0, 60:0, 70:0}
purchases = {20:0, 30:0, 40:0, 50:0, 60:0, 70:0}
totalPurchases = 0
for _ in range(100000):
    ageDecade = random.choice([20, 30, 40, 50, 60, 70])
    purchaseProbability = 0.20# float(ageDecade) / 100.0
    totals[ageDecade] += 1
    if (random.random() < purchaseProbability):
        totalPurchases += 1
        purchases[ageDecade] += 1


print(totals)
print(purchases)

pef = float(purchases[30]) / float(totals[30])
pe = float(totalPurchases) / 100000
print('TOTAL P: %s' % totalPurchases)
print('PEF: %s' % str(pef))
print('PE: %s'  % str(pe))

print ('%s' % str(pef-pe))