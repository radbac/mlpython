import numpy as np
import matplotlib.pyplot as plt
from scipy import stats
import random


page_speed = np.random.normal(3.0, 1.0, 1000)

purchase_amount = 100 - (page_speed + np.random.normal(0, random.random(), 1000)) * 3

plt.scatter(page_speed, purchase_amount)
plt.title('Linear regression example')
plt.xlabel('Page speed')
plt.ylabel('Purchase amount $')


slope, intercept, r_value, p_value, std_err = stats.linregress(page_speed, purchase_amount)

print(r_value ** 2)


def predict(x):
    return slope * x + intercept


fit_line = predict(page_speed)
plt.plot(page_speed, fit_line, c='r')

plt.show()
